#include<stdio.h>
int row()
{
    int a;
    printf("enter no of rows :");
    scanf("%d",&a);
    return a;
}
int column()
{
    int b;
    printf("enter no of columns :");
    scanf("%d",&b);
    return b;
}
int main()
{
    int i,r,c,j;
    r=row();
    c=column();
    int a[r][c];
    printf("enter elements :\n");
    for(i=0;i<r;i++)
    {
       for(j=0;j<c;j++)
       {
           scanf("%d",&a[i][j]);
       }
    }
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d \t",a[i][j]);
        }
        printf("\n");
    }
    printf("transpose of the given matrix :\n");
    for(j=0;j<c;j++)
    {
        for(i=0;i<r;i++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    return 0;
}