#include<stdio.h>
int row()
{
    int a;
    printf("enter no of salesmen :");
    scanf("%d",&a);
    return a;
}
int column()
{
    int b;
    printf("enter no of products :");
    scanf("%d",&b);
    return b;
}
int main()
{
    int i,r,c,j;
    r=row();
    c=column();
    int a[r][c];
    printf("enter no of sales in row wise :\n");
    for(i=0;i<r;i++)
    {
       for(j=0;j<c;j++)
       {
           scanf("%d",&a[i][j]);
       }
    }
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d \t",a[i][j]);
        }
        printf("\n");
    }
    for(i=0;i<r;i++)
    {
        int tot=0;
        for(j=0;j<c;j++)
        {
            tot+=a[i][j];
        }
        printf("total sales by salesman %d=%d\n",(i+1),tot);
    }
    for(j=0;j<c;j++)
    {
        int sum=0;
        for(i=0;i<r;i++)
        {
            sum+=a[i][j];
        }
        printf("total sales of product %d=%d\n",(j+1),sum);
    }
    return 0;
}    