#include<stdio.h>
int num()
{
    int a;
    printf("enter number of elements in array :");
    scanf("%d",&a);
    return a;
}
int main()
{
    int n,i;
    n=num();
    int a[n],max,min,maxpos,minpos,tem;
    printf("enter array elements :\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("array elements before interchanging :");
    for(i=0;i<n;i++)
    {
        printf("%d",a[i]);
        printf("\t");
    }
    max=a[0];
    min=a[0];
    maxpos=0;
    minpos=0;
    for(i=1;i<n;i++)
    {
        if(a[i]>max)
        {
            max=a[i];
            maxpos=i;
        }
        if(a[i]<min)
        {
            min=a[i];
            minpos=i;
        }
    }
    tem=a[maxpos];
    a[maxpos]=a[minpos];
    a[minpos]=tem;
    printf("\n array elements after interchanging :");
    for(i=0;i<n;i++)
    {
        printf("%d",a[i]);
        printf("\t");
    }
  return 0;
}