#include<stdio.h>
int input()
{
    int a;
    printf("enter a number:");
    scanf("%d",&a);
    return a;
}
int sum(int *x,int *y)
{
    int s;
    s=*x+*y;
    return s;
}
int sub(int *x,int *y)
{
    int diff;
    diff=*x-*y;
    return diff;
}
int pro(int *x,int *y)
{
    int mul;
    mul=(*x)*(*y);
    return mul;
}
float div(int *x,int *y)
{
    float d=*x/(float)*y;
    return d;
}
int rem(int *x,int *y)
{
    int r=*x%(*y);
    return r;
}
int main()
{
    int a,b,s,st,m,r;
    float d;
    a=input();
    b=input();
    s=sum(&a,&b);
    printf("Sum=%d",s);
    st=sub(&a,&b);
    printf("\nSubtraction=%d",st);
    m=pro(&a,&b);
    printf("\nProduct=%d",m);
    d=div(&a,&b);
    printf("\nDivision=%0.3f",d);
    r=rem(&a,&b);
    printf("\nRemainder=%d",r);
    return 0;
}