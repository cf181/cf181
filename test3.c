#include<stdio.h>
#include<math.h>
int input()
{
  float a;
  scanf ("%f", &a);
  return a;
}

float dis(float b, float c, float d)
{
  float D;
  D = (c * c) - (4 * b * d);
  return D;
}

float root1(float c, float D, float b)
{
  float r1;
  if (D > 0)
    {
      r1 = (-c + sqrt (D)) / 2 * b;
      return r1;
    }
  if (D < 0)
    {
      r1 = (-c + sqrt (-D)) / 2 * b;
      return r1;
    }
}

float root2(float c, float D, float b)
{
  float r2;
  if (D > 0)
    {
      r2 = (-c - sqrt (D)) / 2 * b;
      return r2;
    }
  if (D < 0)
    {
      r2 = (-c - sqrt (-D)) / 2 * b;
      return r2;
    }
}
int nr(float D)
{
  if (D > 0)
    return 0;
  if (D == 0)
    return 1;
  if (D < 0)
    return 2;
}

void output(float r1, float r2, float D,float x,float y)
{
  int N = nr(D);char i;
  if(N==0)
    {
      printf ("roots are real and distinct\n");
      printf ("roots are %f,%f", r1, r2);
    }
  else if(N==1)
    {
      printf ("roots are real and equal\n");
      printf ("roots are %f,%f", r1, r2);
    }
  else
    {
      printf ("roots are imaginary\n");
      printf ("roots are %0.3f+i%0.3f and %0.3f-i%0.3f",(float)(-y/(2*x)),(float)(sqrt(-D)/(2*x)),(float)(-y/(2*x)),(float)(sqrt(-D)/(2*x)));
    }
}

int
main ()
{
  float x, y, z, di, R1, R2;
  printf("Enter the coefficent of X^2\n");
  x = input();
  printf("Enter the coefficent of X\n");
  y = input();
  printf("Enter the constant term\n");
  z = input();
  di = dis(x, y, z);
  R1 = root1(y, di, x);
  R2 = root2(y, di, x);
  output(R1, R2, di,x,y);
  return 0;
}
