#include<stdio.h>
int input()
{
  int a;
  printf("enter number of rows:");
  scanf("%d",&a);
  return a;
}
void output(int x)
{
  int i,j,k;
  for(i=1;i<=x;i++)
  {
    k=x-1;
    while(k>=i)
	{
	 printf(" ");
	 k--;
	}
	for(j=1;j<=i;j++)
	{
	  printf("%d",j);
	}
	printf("\n");
  }	
}
int main()
{
  int t;
  t=input();
  output(t);
  return 0;
}
