#include<stdio.h>
int input()
{
  int n;
  printf("enter integer:");
  scanf("%d",&n);
  return n;
}
int rev(int a)
{
  int rev=0;
  while(a!=0)
  {
   int rem=a%10;
   rev=rev*10+rem;
   a=a/10;
  }
  return rev;
}
void output(int i,int r)
{
  printf("reverse of %d is %d",i,r);
}
int main()
{ 
  int x,y;
  x=input();
  y=rev(x);
  output(x,y);
  return 0;
}  