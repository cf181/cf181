#include<stdio.h>
float input()
{
    float a;
    printf("enter a number:");
    scanf("%f",&a);
    return a;
}
void swap(float *x,float *y)
{
    float s;
    s=*x;
    *x=*y;
    *y=s;
}
void output(float c,float d)
{
    printf("\nValues after swapping:%0.3f,%0.3f",c,d);
}
int main()
{
    float a,b;
    a=input();
    b=input();
    printf("Values before swapping:%0.3f,%0.3f",a,b);
    swap(&a,&b);
    output(a,b);
    return 0;
}