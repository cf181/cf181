#include<stdio.h>
#include<math.h>
float abscissa()
{
  float x;
  printf("enter the value of x:\n");
  scanf("%f",&x);
  return x;
}
float ordinate()
{
  float y;
  printf("enter the value of y:\n");
  scanf("%f",&y);
  return y;
}
float dist(float a,float b,float c,float d)
{
  float dis=sqrt((c-a)*(c-a)+(d-b)*(d-b));
  return dis;
}
void output(float dis)
{
  printf("distance between the two points:%f",dis);
}
int main()
{
  float X1,Y1,X2,Y2,distance;
  X1=abscissa();
  Y1=ordinate();
  X2=abscissa();
  Y2=ordinate();
  distance=dist(X1,Y1,X2,Y2);
  output(distance);
  return 0;
}  
  
  